job "automation_home_lan" {
    datacenters = ["primary"]
    type = "service"

    group "n8nio" {
        network {
            port "http" {
                to = 5678
                host_network = "private"
            }
        }

        service {
            provider = "nomad"
            port = "http"
            name = "n8nio"

            tags = [
                "traefik.enable=true",
                "traefik.http.routers.n8nio.rule=Host(`automation.home.lan`)"
            ]

            check {
                name = "n8n HTTP"
                type = "tcp"
                port = "http"
                interval = "10s"
                timeout = "2s"
            }

            check_restart {
                limit = 3
                grace = "90s"
                ignore_warnings = false
            }
        }

        task "n8n" {
            driver = "docker"

            config {
                image = "n8nio/n8n:0.230.1"
                network_mode="bridge"

                ports = ["http"]

                volumes = [
                    "/opt/nomad/storage/automation.home.lan/n8n.home:/home/node/.n8n"
                ]
            }

            env {
                TZ="America/Los_Angeles"
                GENERIC_TIMEZONE="America/Los_Angeles"
                N8N_METRICS=true
                N8N_METRICS_INCLUDE_API_ENDPOINTS=true
                N8N_HIRING_BANNER_ENABLED=false
                N8N_EDITOR_BASE_URL="http://automation.home.lan/"
                WEBHOOK_URL="http://automation.home.lan/"
                N8N_DIAGNOSTICS_ENABLED=false
            }
        }
    }
}
